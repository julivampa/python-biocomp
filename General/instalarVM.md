## :computer: Instrucciones para correr la máquina virtual en su computadora

* Descargue el archivo `CursoPython.vdi` desde cualquiera de los siguientes links, [64 bits](https://mega.nz/#!9Cg1nCbD) ó [32 bits](https://mega.nz/#!gOoDSIhA)
, según sea su computadora de 64 o 32 bits (y si tiene dudas, intente con la versión 64 bits primero). Si la página de descarga le pide una clave, utilice la siguiente:

```
64bits -> jGERL9Ux9d2PlISzsNp3AQwrSllmsSZQ08LyD_Wvepk
32bits -> w91ENyQWfv2j1RhAou_lFtY2JxygZknKytMfzApKBvI
```


* [¿Cómo saber si mi computadora es 32 o 64 bits?](https://www.google.com/search?q=como+saber+si+mi+computadora+es+32+o+64+bits&oq=como+saber+si+mi+compu&aqs=chrome.3.69i64j0l5.10731j0j0&sourceid=chrome&ie=UTF-8)
* El archivo real de la máquina virtual pesa alrededor de 12 GB, pero el que descargarán pesa sólo ~5GB porque está comprimido (`zipeado`, termina  en `.gz`). Descomprima el archivo (por ejemplo, en Linux Ubuntu, haciendo clic con el botón derecho sobre el ícono y seleccionando luego `Extraer aquí`) para obtener el archivo `.vdi` concreto.
* Descargue en su computadora la aplicación [Oracle Virtual Box](https://www.virtualbox.org/wiki/Downloads), que le permitirá ejecutar la máquina virtual. En la lista de arriba de todo en esa página hay versiones para Windows, OS X (Mac) y Linux; descargue la que le corresponda.
* Además, debe instalar una extensión para poder realizar acciones adicionales que mejorarán el funcionamiento de la VM. Puede descargarla en [Extention Pack para todas las plataformas](https://download.virtualbox.org/virtualbox/6.0.12/Oracle_VM_VirtualBox_Extension_Pack-6.0.12.vbox-extpack).
* Instale la aplicación recién descargada en su computadora y ábrala.
* Cree una nueva máquina (cliquee el botón `New`) con la siguiente configuración:
    ![](Clase_I/imgs/vbox-new.png)
    * Name and operating system:
        * Name: `CursoPython`
        * Type: `Linux`
        * Version: `Ubuntu (64-bit)` ó `Ubuntu (32-bit)`, según corresponda.
        * Cliquee **Next >**.
    * Memory size:
        * Dependiendo de la cantidad de memoria RAM libre en su máquina,
          arrastre el cosito hasta un valor dentro de la barra verde,
          idealmente 1024 MB o más.
    * Hard disk:
        * Seleccione `Use an existing virtual hard disk file`
        * Cliquee el ícono de la carpeta y elija el archivo `.vdi`
          que copió en su máquina.
          IMPORTANTE: **no** le conviene usar directamente el archivo desde
          el pen drive o disco externo, sino desde una copia en su disco rígido.
          La velocidad de lectura y escritura al USB es más lenta.
        * Cliquee **Create**.
* Debería ver una nueva máquina llamada `CursoPython` en la lista ahora.
  Ahora seleccione la máquina virtual `CursoPython` y cliquee el botón `Settings` -> `Network`. Una vez ahí, seleccione la opción `Enable Network Adapter`. Luego, en el campo `Attached to:` elegir la opción `Bridged Adapter`.
Una vez realizado lo anterior, verá algo como lo siguiente:
![](Clase_I/imgs/network_VM.png)
* Estando su computadora conectada a internet, en la opción `Name:` seleccione la que aparezca con el prefijo `wlp` seguido (o no) de 3 o 4 caracteres más como se ve en la imagen de arriba,
 y trate de dejar las demás opciones de forma similar a la imagen.
* Ahora, aún estando en `Settings`, entrar en el campo `Display` (panel de la izquierda `Settings` -> `Display`). En la opción `Video Memory:` subir el valor hasta casi el máximo, intentando que como mínimo tenga 64MB. Además, seleccione la opción `Enable 3D Acceleration` siempre y cuando aparezca pero ninguna otra más. La siguiente figura es un ejemplo de como tendría que quedar:
![](Clase_I/imgs/screen_VM.png)
* Cliquee `Ok`.
* Cliquee la flecha verde de `Start`.
* Es posible que la máquina arranque en una resolución baja por default.
  Mientras está corriendo, haga click derecho en el ícono del monitor que aparece abajo a la derecha. 
Le permitirá elegir `Virtual Screen 1` -> `Resize to 1280 x 720`, u otra resolución que se vea bien en su monitor.
Reinicie la máquina virtual (apagándola desde adentro de Ubuntu alcanza) y arránquela de nuevo: la resolución elegida debería aplicarse.
Si lo anterior no funciona, puede seleccionando `Virtual Screen 1` -> `Auto-resize Guest Display`.
