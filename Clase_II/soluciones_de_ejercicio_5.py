#Ejercicio 1
#Escriba en mayúsculas la siguiente cadena de texto o string:
nombres = 'luis,federico,leloir'
print(nombres.upper())
#Ejercicio 2
#Divida la anterior cadena de texto en una lista de palabras.
nombres=nombres.split(",")
print(nombres)
#Ejercicio 3
#Guarde cada palabra de la cadena de texto anterior como una variable diferente.
primer_nombre=nombres[0]
segundo_nombre=nombres[1]
apellido=nombres[2]
#Ejercicio 4
#Elimine los espacios y retornos de carro extra de la siguiente cadena de texto:
saludo = '   hola \n'
print(saludo)
saludo1 = saludo.strip()
print(saludo1)
#Ejercicio 5
#Una la siguiente lista de palabras en una cadena de texto en la que las palabras estén separadas por comas:
palabras = ['una', 'lista', 'con', 'palabras']
palabras1 = palabras[0] + "," + palabras[1] + "," + palabras[2] + "," + palabras[3]
print(palabras1)
#Ejercicio 6
#Reemplace las comas por tabuladores en la siguiente cadena de texto:
numeros = 'una,dos,tres,cuatro'
print(numeros)
numeros1 = numeros.replace(",", "   ")
print(numeros1)
#Ejercicio 7
#¿Está la palabra “uno” contenida en las siguientes cadenas de texto?
string1 = 'tres, dos, uno'
string2 = 'spam, spam, spam'
print("uno" in string1)
print("uno" in string2)
#Ejercicio 8
#Seleccione los primeros tres elementos de la siguiente línea. Después, los tres últimos.
lista2 = ['uno', 'dos', 'tres', 'cuatro', 5, 6, 7]
print(lista2[:3])
print(lista2[-3:])
#Ejercicio 9
#Calcule el número de elementos de los que se compone la lista anterior.
print(len(lista2))
#Ejercicio 10
#Dada la siguiente lista, calcule el número de elementos no repetitivos de la misma.
lista3 = ['uno', 'dos', 'tres', 'dos', 4, 5, 6, 6, 7, 'tres']
print(set(lista3))
#Ejercicio 11
#¿Cuál será el resultado que obtendremos al finalizar la ejecución de las cuatro líneas de código siguientes?
a = [1, 2, 3]
b = a
b.append(4) #el .append() agrega el 4 a la lista llamada "a"
print(a)
#Ejercicio 12
#¿Qué pasaría si ejecutásemos el siguiente código? ¿Por qué se observa un comportamiento diferente al del ejercicio anterior?
a = '123'
b = a
b = a + '4' #se está concatenando y no agregando un elemento
print(b)
#Ejercicio 13
#¿Comienzan las siguientes cadenas de texto por “#”? Si es así, elimine ese caracter.
string3 = '#explicacion de los campos'
string4 = 'campo1, campo2, campo2'
string3_sin_numeral = string3[1:]
print(string3_sin_numeral)
print(string3)
#Ejercicio 14
#Dada la siguiente lista de cadenas de texto, filtre las que comienzan por “#” y obtenga una nueva lista con las cadenas filtradas.
lista1 = ["#explicacion", "campos", "#campo1", "campo2", "campo2"]
#solucion 0 lista de las que empiezan con "#"
lista_filtrada_0 = []
for i in lista1:
    if i[0] == "#":
        lista_filtrada_0.append(i)
print(lista_filtrada_0)
#Ejercicio 15
#Imprima las claves o keys del siguiente diccionario. Luego, imprima sus valores.
d = {1 : 'Noether',3: 'Isaac', 2: 'Albert' }
print(d.keys())
print(d.values())
#Ejercicio 16
#Realice lo mismo que en el ejercicio anterior, pero ahora imprima las claves en orden (recuerde que los diccionarios no necesariamente estarán ordenados). Después, imprima sus valores ordenados.
print("ejercicio 16, diccionario ordenado")
print(sorted(d.keys())) #ordena keys
print(sorted(d.values())) #ordena las values
#Ejercicio 17
#¿Está el valor ‘hola’ en el siguiente diccionario?
d = {"spam": "hola", "monty": "spam"}
print("hola" in d.values()) #busca los values del diccionario llamado "d"
